package info.androidhive.materialdesign.util;

/**
 * Created by Ibrahim.Abdelgawad on 8/25/2015.
 */
public class Utility
{
    public static String convert(byte[] data) {
        StringBuilder sb = new StringBuilder(data.length);
        for (int i = 0; i < data.length; ++ i) {
            if (data[i] < 0) throw new IllegalArgumentException();
            sb.append((char) data[i]);
        }
        return sb.toString();
    }
}
