package info.androidhive.materialdesign.model;

/**
 * Created by Ibrahim.Abdelgawad on 8/16/2015.
 */
public class Contact
{
    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {

        return number;
    }

    public String getName() {

        return name;
    }

    String name = "";
    String number = "";
}
