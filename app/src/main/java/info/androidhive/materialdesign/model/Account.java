package info.androidhive.materialdesign.model;

/**
 * Created by Ibrahim.Abdelgawad on 8/25/2015.
 */
public class Account
{
    private String accountNumber;
    private String accountNickName;
    private String accountType;
    private String accountBranch;
    private String accountCurrency;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNickName() {
        return accountNickName;
    }

    public void setAccountNickName(String accountNickName) {
        this.accountNickName = accountNickName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountBranch() {
        return accountBranch;
    }

    public void setAccountBranch(String accountBranch) {
        this.accountBranch = accountBranch;
    }

    public String getAccountCurrency() {
        return accountCurrency;
    }

    public void setAccountCurrency(String accountCurrency) {
        this.accountCurrency = accountCurrency;
    }

    public String getAccountAvailableBalance() {
        return accountAvailableBalance;
    }

    public void setAccountAvailableBalance(String accountAvailableBalance) {
        this.accountAvailableBalance = accountAvailableBalance;
    }

    public String getAccountBalanceType() {
        return accountBalanceType;
    }

    public void setAccountBalanceType(String accountBalanceType) {
        this.accountBalanceType = accountBalanceType;
    }

    private String accountAvailableBalance;
    private String accountBalanceType;

}
