package info.androidhive.materialdesign.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.viewpagerindicator.CirclePageIndicator;

import info.androidhive.materialdesign.R;
import info.androidhive.materialdesign.adapter.TourAdapter;

/**
 * Created by Ibrahim.Abdelgawad on 8/6/2015.
 */
public class TourFragment extends FragmentActivity{
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tour_layout);

        viewPager = (ViewPager) findViewById(R.id.mpager);
        viewPager.setAdapter(new TourAdapter(getSupportFragmentManager(), getApplicationContext()));
        viewPager.setCurrentItem(0);

        CirclePageIndicator mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(viewPager);
        mIndicator.setSnap(true);

    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
