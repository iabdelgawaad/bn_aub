package info.androidhive.materialdesign.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import info.androidhive.materialdesign.R;

/**
 * Created by Ibrahim.Abdelgawad on 8/6/2015.
 */
public class ToutorialImageFragment extends Fragment{

    final int[] tutImgs = new int[]{R.drawable.visa_logo,
            R.drawable.ic_profile, R.drawable.visa_logo,R.drawable.ic_profile };

    public static ToutorialImageFragment newInstance(int indx) {
        ToutorialImageFragment fragment = new ToutorialImageFragment();
        Bundle b = new Bundle();
        b.putInt("indx", indx);
        fragment.setArguments(b);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ImageView iv = (ImageView) inflater.inflate(R.layout.toutorial_img,
                null, false);
        iv.setImageBitmap(null);
        iv.setBackgroundColor(Color.BLACK);
        iv.setImageResource(tutImgs[getArguments().getInt("indx")]);
        return iv;
    }
}
