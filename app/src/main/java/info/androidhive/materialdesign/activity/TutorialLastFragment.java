package info.androidhive.materialdesign.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

/**
 * Created by Ibrahim.Abdelgawad on 8/6/2015.
 */
public class TutorialLastFragment extends Fragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static TutorialLastFragment newInstance() {
        TutorialLastFragment fragment = new TutorialLastFragment();
        Bundle b = new Bundle();
        fragment.setArguments(b);
        return fragment;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().finish();
    }
}
