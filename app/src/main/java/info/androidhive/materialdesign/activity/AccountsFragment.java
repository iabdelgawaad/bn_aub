package info.androidhive.materialdesign.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.loopj.android.http.*;


import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.materialdesign.R;
import info.androidhive.materialdesign.adapter.AccountAdapter;
import info.androidhive.materialdesign.adapter.ContactInfo;
import info.androidhive.materialdesign.model.Account;
import info.androidhive.materialdesign.model.Contact;
import info.androidhive.materialdesign.util.ServiceHandlerHttp;
import info.androidhive.materialdesign.util.Utility;

/**
 * Created by Ravi on 29/07/15.
 */
//http://www.binpress.com/tutorial/android-l-recyclerview-and-cardview-tutorial/156

public class AccountsFragment extends Fragment {
    protected List<Contact> contacts;
    private static final int DATASET_COUNT = 60;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    //private  String url = "http://localhost:9080/bn/rs/cp/accounts";
    private String url = "http://10.3.7.121:9080/bn/rs/cp/accounts";

    private String result_key = "result";
    private ProgressDialog pDialog;
    private static final String TAG = "Accounts ::";
    ArrayList<Account> accountArrayList = new ArrayList<Account>();
    RecyclerView recList;



    public AccountsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_accounts, container, false);

        recList = (RecyclerView) rootView.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        //get accounts and set in model classes
        getAccounts();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*GetUserAccounts getUserAccounts = new GetUserAccounts();
        getUserAccounts.execute();*/

    }

    public void getAccounts() {
        // Creating service handler class instance
        ServiceHandlerHttp sh = new ServiceHandlerHttp();

           /* // Making a request to url and getting response
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            //nameValuePairs.add(new BasicNameValuePair("Authorization", "Basic YW1pcmFhaG1lZDphbWlyYWFobWVk"));
            String jsonStr = sh.makeServiceCall(url, ServiceHandlerHttp.GET,nameValuePairs);//adding params*/


        //
        AsyncHttpClient client = new AsyncHttpClient();
        //client.addHeader("token", token);
        //client.addHeader("userId",userID);

        client.addHeader("Authorization", "Basic YW1pcmFhaG1lZDphbWlyYWFobWVk");

        client.get(getActivity().getApplicationContext(), url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String jsonStr = Utility.convert(responseBody);
                Log.d(TAG, jsonStr);


                try {
                    JSONObject jsonObject = new JSONObject(jsonStr);
                    Log.v(TAG, "jsonObject.toString() :: " + jsonObject.toString());

                    if (jsonObject.getString("result").trim().equalsIgnoreCase("1")) {
                        Toast.makeText(getActivity().getApplicationContext(), "Accounts return successfully", Toast.LENGTH_SHORT).show();

                        String accounts = jsonObject.getString("accounts");
                        JSONArray accountJsonArray = new JSONArray(accounts);
                        Log.v(TAG, "accountsJsonArray.toString() :: " + accountJsonArray.toString());


                        for (int i = 0; i < accountJsonArray.length(); i++) {
                            Account account = new Account();

                            account.setAccountNickName(accountJsonArray.getJSONObject(i).getString("acnt_nickname"));
                            account.setAccountNumber(accountJsonArray.getJSONObject(i).getString("acnt_number"));
                            account.setAccountCurrency(accountJsonArray.getJSONObject(i).getString("acnt_currency"));
                            account.setAccountAvailableBalance(accountJsonArray.getJSONObject(i).getString("acnt_balance"));
                            account.setAccountBranch(accountJsonArray.getJSONObject(i).getString("branchName"));
                            account.setAccountType(accountJsonArray.getJSONObject(i).getString("acnt_type"));
                            account.setAccountBalanceType(accountJsonArray.getJSONObject(i).getString("acnt_balanceType"));


                            accountArrayList.add(i, account);

                        }

                        recList.setAdapter(new AccountAdapter(accountArrayList));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getActivity().getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();

            }
        });

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private List<ContactInfo> createList(int size) {

        List<ContactInfo> result = new ArrayList<ContactInfo>();
        for (int i = 1; i <= size; i++) {
            ContactInfo ci = new ContactInfo();
            ci.name = ContactInfo.NAME_PREFIX + i;
            ci.surname = ContactInfo.SURNAME_PREFIX + i;
            ci.email = ContactInfo.EMAIL_PREFIX + i + "@test.com";

            result.add(ci);
        }

        return result;
    }

    private class GetUserAccounts extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandlerHttp sh = new ServiceHandlerHttp();

            String strJson = "";
           /* // Making a request to url and getting response
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            //nameValuePairs.add(new BasicNameValuePair("Authorization", "Basic YW1pcmFhaG1lZDphbWlyYWFobWVk"));
            String jsonStr = sh.makeServiceCall(url, ServiceHandlerHttp.GET,nameValuePairs);//adding params*/


            //
            AsyncHttpClient client = new AsyncHttpClient();
            //client.addHeader("token", token);
            //client.addHeader("userId",userID);
            Header header1 = new BasicHeader("Authorization", "Basic YW1pcmFhaG1lZDphbWlyYWFobWVk");
            Header[] headers = {header1};


            client.setBasicAuth("Authorization", "Basic YW1pcmFhaG1lZDphbWlyYWFobWVk");
            client.get(getActivity().getApplicationContext(), url, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String jsonStr = Utility.convert(responseBody);
                    Log.d(TAG, jsonStr);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                }
            });

            return strJson;
        }

        @Override
        protected void onPostExecute(String jsonStr) {
            super.onPostExecute(jsonStr);
            // Dismiss the progress dialog
            Log.d("ana", "hna");
            if (pDialog.isShowing())
                pDialog.dismiss();

            //

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String status = jsonObj.getJSONArray(result_key).getJSONObject(0).getString("status");
                    if (status.equals("1")) {     //correct


                    } else if (status.equals("0")) {//error email

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            //

        }

    }
}
