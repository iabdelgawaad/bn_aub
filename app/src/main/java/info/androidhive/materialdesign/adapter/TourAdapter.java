package info.androidhive.materialdesign.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import info.androidhive.materialdesign.activity.HomeFragment;
import info.androidhive.materialdesign.activity.MainActivity;
import info.androidhive.materialdesign.activity.Splash;
import info.androidhive.materialdesign.activity.TourFragment;
import info.androidhive.materialdesign.activity.ToutorialImageFragment;
import info.androidhive.materialdesign.activity.TutorialLastFragment;

/**
 * Created by Ibrahim.Abdelgawad on 8/6/2015.
 */
public class TourAdapter extends FragmentPagerAdapter{
    Context mcontext;

    public TourAdapter (FragmentManager fm , Context context) {
        super(fm);
        mcontext = context;
        // TODO Auto-generated constructor stub
    }

    @Override
    public Fragment getItem(int arg0) {
        // TODO Auto-generated method stub
        if (arg0 != 3)
            return ToutorialImageFragment.newInstance(arg0);

        else {
            Intent intent = new Intent(mcontext, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mcontext.startActivity(intent);
        }
        return TutorialLastFragment.newInstance();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 4;
    }
}
