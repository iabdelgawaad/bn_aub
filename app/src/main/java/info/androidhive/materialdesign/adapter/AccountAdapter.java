/*
 * Copyright (C) 2014 Francesco Azzola
 *  Surviving with Android (http://www.survivingwithandroid.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package info.androidhive.materialdesign.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import info.androidhive.materialdesign.R;
import info.androidhive.materialdesign.model.Account;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.ContactViewHolder> {

    private List<Account> accountsList;

    public AccountAdapter(List<Account> accountsList) {
        this.accountsList = accountsList;
    }


    @Override
    public int getItemCount() {
        return accountsList.size();
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        Account acct = accountsList.get(i);
        contactViewHolder.vNickName.setText(acct.getAccountNickName());
        contactViewHolder.vAvailable_Balance.setText(acct.getAccountAvailableBalance());
        contactViewHolder.vBalance_Type.setText(acct.getAccountBalanceType());
        contactViewHolder.vAccount_Number.setText(acct.getAccountNumber());
        contactViewHolder.vCurrency.setText(acct.getAccountCurrency());
        contactViewHolder.vBranch.setText(acct.getAccountBranch());
        contactViewHolder.vAccount_Type.setText(acct.getAccountType());

    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.account_card_layout, viewGroup, false);

            return new ContactViewHolder(itemView);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        protected TextView vNickName;
        protected TextView vType;
        protected TextView vBranch;
        protected TextView vCurrency;
        protected TextView vAvailable_Balance;
        protected TextView vBalance_Type;
        protected TextView vAccount_Number;
        protected TextView vAccount_Type;



        public ContactViewHolder(View v) {
            super(v);
            vNickName =  (TextView) v.findViewById(R.id.txt_nickname_value);
            vType = (TextView)  v.findViewById(R.id.txt_type_value);
            vBranch = (TextView)  v.findViewById(R.id.txt_branch_value);
            vCurrency = (TextView) v.findViewById(R.id.txt_currency_value);
            vAvailable_Balance = (TextView) v.findViewById(R.id.txt_available_balance_value);
            vBalance_Type = (TextView) v.findViewById(R.id.txt_balance_type_value);
            vAccount_Number = (TextView) v.findViewById(R.id.txt_accountNumber);
            vAccount_Type = (TextView) v.findViewById(R.id.txt_type_value);
        }
    }
}
